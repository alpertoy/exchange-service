# Currency exchange service

Available currencies are USD and EUR

## How to run

Cd to the app folder and run

``
docker-compose up
``

## How to test

Currency exchange from USD to EUR

``
http://localhost:8100/currency-conversion/from/USD/to/EUR/quantity/1
``

Currency exchange from EUR to USD

``
http://localhost:8100/currency-conversion/from/EUR/to/USD/quantity/1
``
