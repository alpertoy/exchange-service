package io.gitlab.alpertoy.exchange.currencyexchange.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.gitlab.alpertoy.exchange.currencyexchange.entity.CurrencyExchange;
import io.gitlab.alpertoy.exchange.currencyexchange.repository.CurrencyExchangeRepository;
import io.gitlab.alpertoy.exchange.currencyexchange.service.CurrencyExchangeService;

@Service
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {
	
	@Autowired
	private CurrencyExchangeRepository repository;
	
	@Autowired
	private Environment environment;
	
	@Override
	@RateLimiter(name = "default")
	public CurrencyExchange retrieveExchangeValue(String from, String to) {
		
		CurrencyExchange currencyExchange = repository.findByFromAndTo(from, to);
		
		String port = environment.getProperty("local.server.port");
		currencyExchange.setEnvironment(port);
		
		return currencyExchange;
	}

}
