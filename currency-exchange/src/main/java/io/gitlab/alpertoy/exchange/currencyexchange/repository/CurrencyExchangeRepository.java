package io.gitlab.alpertoy.exchange.currencyexchange.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.gitlab.alpertoy.exchange.currencyexchange.entity.CurrencyExchange;

public interface CurrencyExchangeRepository extends JpaRepository<CurrencyExchange, Long> {
	
	CurrencyExchange findByFromAndTo(String from, String to);

}
