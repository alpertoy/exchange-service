package io.gitlab.alpertoy.exchange.currencyexchange.service;

import io.gitlab.alpertoy.exchange.currencyexchange.entity.CurrencyExchange;

public interface CurrencyExchangeService {
	
	CurrencyExchange retrieveExchangeValue(String from, String to);

}
